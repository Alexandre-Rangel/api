<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRotinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rotinas', function (Blueprint $table) {
            $table->id();
            $table->text('descricao');
            $table->foreignId('tipo_rotina_id')->constrained('tipos_rotinas');
            $table->enum('status', ['concluído', 'em andamento', 'parado', 'em espera']);
            $table->timestamp('tempo')->nullable();
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rotinas');
    }
}
