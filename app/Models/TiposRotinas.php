<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiposRotinas extends Model
{
    use HasFactory;
    protected $table = 'tipos_rotinas'; 
    protected $fillable = ['tipo'];
}
