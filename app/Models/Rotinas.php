<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rotinas extends Model
{
    use HasFactory;
    protected $table = 'rotinas'; 
    protected $fillable = [
        'descricao','tipo_rotina_id',
        'status','tempo'];
}
