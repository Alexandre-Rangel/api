<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rotinas;

class RotinaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $rotinas = Rotinas::all();
        return response()->json(['rotinas' => $rotinas], 200);
    }

    public function store(Request $request)
    {
        // Validar os dados recebidos do request
        $request->validate([
            'descricao' => 'required|string',
            'tipo_rotina_id' => 'required|exists:tipos_rotinas,id',
            'status' => 'required|in:concluído,em andamento,parado,em espera',
            'tempo' => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        // Criar uma nova rotina
        $rotina = Rotinas::create([
            'descricao' => $request->descricao,
            'tipo_rotina_id' => $request->tipo_rotina_id,
            'status' => $request->status,
            'tempo' => $request->tempo
        ]);

        if (!$rotina) {
            return response()->json(['message' => 'Não foi possível criar a rotina'], 500);
        }

        return response()->json(['rotina' => $rotina], 201);
    }

    public function update(Request $request, $id)
    {
        // Validar os dados recebidos do request
        $request->validate([
            'descricao' => 'required|string',
            'tipo_rotina_id' => 'required|exists:tipos_rotinas,id',
            'status' => 'required|in:concluído,em andamento,parado,em espera',
            'tempo' => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        $rotina = Rotinas::find($id);

        if (!$rotina) {
            return response()->json(['message' => 'Rotina não encontrada'], 404);
        }

        $updated = $rotina->update([
            'descricao' => $request->descricao,
            'tipo_rotina_id' => $request->tipo_rotina_id,
            'status' => $request->status,
            'tempo' => $request->tempo
        ]);

        if (!$updated) {
            return response()->json(['message' => 'Não foi possível atualizar a rotina'], 500);
        }

        return response()->json(['rotina' => $rotina], 200);
    }

    public function destroy($id)
    {
        
        $rotina = Rotinas::find($id);

        if (!$rotina) {
            return response()->json(['message' => 'Rotina não encontrada'], 404);
        }

        $deleted = $rotina->delete();

        if (!$deleted) {
            return response()->json(['message' => 'Não foi possível excluir a rotina'], 500);
        }

        return response()->json(['message' => 'Rotina deletada com sucesso'], 200);
    }
}
