<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsuarioRotina;

class UsuarioRotinaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        // Validar os dados recebidos do request
        $request->validate([
            'usuario_id' => 'required|exists:users,id',
            'rotina_id' => 'required|exists:rotinas,id',
        ]);

        // Criar uma nova associação entre usuário e rotina
        $usuarioRotina = UsuarioRotina::create([
            'usuario_id' => $request->usuario_id,
            'rotina_id' => $request->rotina_id,
        ]);

        if (!$usuarioRotina) {
            return response()->json(['message' => 'Não foi possível criar a associação entre usuário e rotina'], 500);
        }

        // Retornar a associação criada
        return response()->json(['usuario_rotina' => $usuarioRotina], 201);
    }

    public function destroy($id)
    {
        $usuarioRotina = UsuarioRotina::find($id);

        if (!$usuarioRotina) {
            return response()->json(['message' => 'Associação usuário-rotina não encontrada'], 404);
        }

        $deleted = $usuarioRotina->delete();

        if (!$deleted) {
            return response()->json(['message' => 'Não foi possível excluir a associação usuário-rotina'], 500);
        }

        return response()->json(['message' => 'Associação usuário-rotina deletada com sucesso'], 200);
    }
}
