<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TiposRotinas;

class TipoRotinaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        // Obter todos os tipos de rotinas
        $tiposRotinas = TiposRotinas::all();
        
        // Retornar os tipos de rotinas como resposta
        return response()->json(['tipos_rotinas' => $tiposRotinas], 200);
    }

    public function store(Request $request)
    {
        // Validar os dados recebidos do request
        $request->validate([
            'tipo' => 'required|string|max:255'
        ]);

        // Criar um novo tipo de rotina
        $tipoRotina = TiposRotinas::create([
            'tipo' => $request->tipo
        ]);
        if (!$tipoRotina) {
            return response()->json(['message' => 'Não foi possível criar o tipo de rotina'], 500);
        }
        // Retornar o tipo de rotina criado
        return response()->json(['tipo_rotina' => $tipoRotina], 201);
    }

    public function update(Request $request, $id)
    {
        // Validar os dados recebidos do request
        $request->validate([
            'tipo' => 'required|string|max:255'
        ]);

        // Encontrar o tipo de rotina pelo ID
        $tipoRotina = TiposRotinas::find($id);

        // Se o tipo de rotina não existir, retornar uma resposta 404
        if (!$tipoRotina) {
            return response()->json(['message' => 'Tipo de rotina não encontrado'], 404);
        }

        // Atualizar o tipo de rotina
        $updated = $tipoRotina->update([
            'tipo' => $request->tipo
        ]);

        if (!$updated) {
            return response()->json(['message' => 'Não foi possível atualizar o tipo de rotina'], 500);
        }

        // Retornar o tipo de rotina atualizado
        return response()->json(['tipo_rotina' => $tipoRotina], 200);
    }

    public function destroy($id)
    {
        // Encontrar o tipo de rotina pelo ID
        $tipoRotina = TiposRotinas::find($id);

        // Se o tipo de rotina não existir, retornar uma resposta 404
        if (!$tipoRotina) {
            return response()->json(['message' => 'Tipo de rotina não encontrado'], 404);
        }

        // Deletar o tipo de rotina
        $deleted = $tipoRotina->delete();

        if (!$deleted) {
            return response()->json(['message' => 'Não foi possível excluir o tipo de rotina'], 500);
        }

        // Retornar uma resposta de sucesso
        return response()->json(['message' => 'Tipo de rotina deletado com sucesso'], 200);
    }
}
