<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RotinaController;
use App\Http\Controllers\TipoRotinaController;
use App\Http\Controllers\UsuarioRotinaController;
 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('login', 'AuthController@login');
Route::post('logout', [AuthController::class, 'logout']);
Route::post('login', [AuthController::class, 'login']);

Route::get('/tipos-rotinas', [TipoRotinaController::class, 'index']);
Route::post('/tipos-rotinas', [TipoRotinaController::class, 'store']);
Route::put('/tipos-rotinas/{id}', [TipoRotinaController::class, 'update']);
Route::delete('/tipos-rotinas/{id}', [TipoRotinaController::class, 'destroy']);

Route::get('/rotinas', [RotinaController::class, 'index']);
Route::post('/rotinas', [RotinaController::class, 'store']);
Route::put('/rotinas/{id}', [RotinaController::class, 'update']);
Route::delete('/rotinas/{id}', [RotinaController::class, 'destroy']);


Route::post('/usuarios-rotinas', [UsuarioRotinaController::class, 'store']);
Route::delete('/usuarios-rotinas/{id}', [UsuarioRotinaController::class, 'destroy']);

Route::middleware('auth.jwt')->get('/rotina', [RotinaController::class, 'rotina']);
